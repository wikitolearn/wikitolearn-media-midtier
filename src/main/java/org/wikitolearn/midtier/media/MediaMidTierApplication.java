package org.wikitolearn.midtier.media;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MediaMidTierApplication {

	public static void main(String[] args) {
		SpringApplication.run(MediaMidTierApplication.class, args);
	}
}
