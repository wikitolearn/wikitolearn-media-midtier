package org.wikitolearn.midtier.media.config;

import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;

@EnableCircuitBreaker
public class HystrixConfiguration {}
